﻿using Microsoft.Extensions.Configuration;


namespace BSS
{
    public static class Common
    {
        public static string GetAppSettings(string key)
        {
            return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build()[key];
        }
    }
}