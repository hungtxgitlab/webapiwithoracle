﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;

namespace BSS
{
    public static class Convertor
    {

        public static string DataTableToList<T>(DataTable table, out List<T>? list)
        {
            list = null;
            if (table == null)
            {
                return "Error: table is null @DataTableToList";
            }

            list = new List<T>();
            string text = "";
            foreach (DataRow row in table.Rows)
            {
                text = RowToObject<T>(row, out T? value);
                if (text.Length == 0 && value == null)
                {
                    text = "Error: item is null @DataTableToList";
                }

                if (text.Length > 0)
                {
                    list.Clear();
                    list = null;
                    return text;
                }

                list.Add(value);
            }

            return text;
        }
        public static string DataTableToObject<T>(DataTable table, out T? output)
        {
            output = default;
            List<T>? list = null;
            if (table == null)
            {
                return "Error: table is null @DataTableToList";
            }

            list = new List<T>();
            string text = "";
            foreach (DataRow row in table.Rows)
            {
                text = RowToObject<T>(row, out T value);
                if (text.Length == 0 && value == null)
                {
                    text = "Error: item is null @DataTableToList";
                }

                if (text.Length > 0)
                {
                    list.Clear();
                    list = null;
                    return text;
                }

                list.Add(value);
            }

            output = list.Count > 0 ? list[0] : default(T);

            return text;
        }

        public static string ObjectToDataTable<T>(T obj, out DataTable? dt)
        {
            dt = null;
            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                dt = new DataTable();
                for (int i = 0; i < properties.Count; i++)
                {
                    dt.Columns.Add(new DataColumn
                    {
                        AllowDBNull = true,
                        ColumnName = properties[i].Name,
                        DataType = (Nullable.GetUnderlyingType(properties[i].PropertyType) ?? properties[i].PropertyType)
                    });
                }

                object[] array = new object[properties.Count];

                for (int j = 0; j < array.Length; j++)
                {
                    if (properties[j].GetValue(obj) != null)
                    {
                        array[j] = properties[j].GetValue(obj);
                    }
                }

                dt.Rows.Add(array);

                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string ListToDataTable<T>(IList<T> list, out DataTable? dt)
        {
            dt = null;
            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                dt = new DataTable();
                for (int i = 0; i < properties.Count; i++)
                {
                    dt.Columns.Add(new DataColumn
                    {
                        AllowDBNull = true,
                        ColumnName = properties[i].Name,
                        DataType = (Nullable.GetUnderlyingType(properties[i].PropertyType) ?? properties[i].PropertyType)
                    });
                }

                object[] array = new object[properties.Count];
                foreach (T item in list)
                {
                    for (int j = 0; j < array.Length; j++)
                    {
                        if (properties[j].GetValue(item) != null)
                        {
                            array[j] = properties[j].GetValue(item);
                        }
                    }

                    dt.Rows.Add(array);
                }

                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string StringRemoveMarkVietnamese(this string input, int type = 0)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string input2 = input.Normalize(NormalizationForm.FormD);
            string text = regex.Replace(input2, string.Empty)
                .Replace('đ', 'd')
                .Replace('Đ', 'D');
            return type switch
            {
                1 => text.ToLower(),
                2 => text.ToUpper(),
                _ => text,
            };
        }

        public static string StringRemoveMarkVietnameseToLower(this string input)
        {
            return input.StringRemoveMarkVietnamese(1);
        }

        public static string StringRemoveMarkVietnameseToUpper(this string input)
        {
            return input.StringRemoveMarkVietnamese(2);
        }

        public static string ObjToString(this object obj)
        {
            if (obj == null)
            {
                return "";
            }

            return obj.ToString();
        }

        public static string ObjToJson(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return "";
                }

                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception)
            {
                return "";
            };
        }

        public static List<object> ObjToList(object obj)
        {
            // Kiểm tra nếu đối tượng không null
            if (obj != null)
            {
                // Sử dụng reflection để lấy danh sách các thuộc tính của đối tượng
                var properties = obj.GetType().GetProperties();

                // Tạo danh sách chứa các đối tượng anonymous type
                var resultList = new List<object>();

                // Lặp qua từng thuộc tính và lấy giá trị của nó
                foreach (var property in properties)
                {
                    var propertyValue = property.GetValue(obj, null);

                    // Thêm thuộc tính và giá trị tương ứng vào anonymous type và thêm vào danh sách
                    resultList.Add(new { PropertyName = property.Name, PropertyValue = propertyValue });
                }

                return resultList;
            }

            // Trả về danh sách trống nếu đối tượng là null
            return new List<object>();
        }

        private static string RowToObject<T>(DataRow row, out T value)
        {
            value = default(T);
            if (row == null)
            {
                return "Error: row is null @RowToObject";
            }

            string text = "";
            try
            {
                value = Activator.CreateInstance<T>();
                Type type = value.GetType();
                foreach (DataColumn column in row.Table.Columns)
                {
                    text = column.ColumnName;
                    PropertyInfo property = type.GetProperty(text, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                    if (property == null)
                    {
                        if (type.Name == "Object")
                        {
                            value = (T)Convert.ChangeType(row[text].ObjToString(), typeof(T));
                            return "";
                        }

                        continue;
                    }

                    object obj = row[text];
                    Type type2 = column.GetType();
                    if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        Type type3 = property.PropertyType.GetGenericArguments()[0];
                        if (type3 != null && type3 == typeof(bool))
                        {
                            object outValue;
                            string value2 = GetValue(property.PropertyType, obj, out outValue);
                            if (value2.Length > 0)
                            {
                                return "Convert value trường " + text + " lỗi: " + value2;
                            }

                            obj = $"{outValue}" != "0";
                        }

                        if (type3 != null && type3 == typeof(byte))
                        {
                            object outValue2;
                            string value2 = GetValue(property.PropertyType, obj, out outValue2);
                            if (value2.Length > 0)
                            {
                                return "Convert value trường " + text + " lỗi: " + value2;
                            }

                            obj = null;
                            if (outValue2 != DBNull.Value)
                            {
                                obj = Convert.ToByte(outValue2);
                            }
                        }
                    }

                    if (obj != DBNull.Value)
                    {
                        object outValue3;
                        string value2 = GetValue(property.PropertyType, obj, out outValue3);
                        if (value2.Length > 0)
                        {
                            return "Convert value trường " + text + " lỗi: " + value2;
                        }

                        property.SetValue(value, outValue3, null);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString() + " @" + text;
            }

            return "";
        }

        private static string GetValue(Type type, object value, out object? outValue)
        {
            outValue = null;
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Int32:
                    {
                        if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                        {
                            int.TryParse(value.ToString(), out var result4);
                            outValue = result4;
                            break;
                        }

                        if (bool.TryParse(value.ToString(), out var result5))
                        {
                            int.TryParse(result5.ToString(), out var result6);
                            outValue = result6;
                            break;
                        }

                        if (!int.TryParse(value.ToString(), out var result7))
                        {
                            return value?.ToString() + " không phải là số nguyên";
                        }

                        outValue = result7;
                        break;
                    }
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    {
                        long.TryParse(value.ToString(), out var result3);
                        outValue = result3;
                        break;
                    }
                case TypeCode.String:
                    outValue = value.ToString();
                    break;
                case TypeCode.Boolean:
                    {
                        if (!bool.TryParse(value.ToString(), out var result8))
                        {
                            if (value.ToString() == "1")
                            {
                                outValue = true;
                                break;
                            }

                            if (!(value.ToString() == "0"))
                            {
                                return value?.ToString() + " không phải là kiểu Boolean";
                            }

                            outValue = false;
                        }
                        else
                        {
                            outValue = result8;
                        }

                        break;
                    }
                case TypeCode.Object:
                    if (value != null && (type == typeof(Guid) || type == typeof(Guid?)))
                    {
                        if (!Guid.TryParse(value.ToString(), out var result2))
                        {
                            return value?.ToString() + " không phải là kiểu Guid";
                        }

                        Guid.TryParse(value.ToString(), out result2);
                        outValue = result2;
                    }
                    else
                    {
                        outValue = value;
                    }

                    break;
                case TypeCode.DateTime:
                    if (value != null)
                    {
                        if (DateTime.TryParseExact(value.ToString(), "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
                        {
                            outValue = result;
                        }
                        else if (DateTime.TryParse(value.ToString(), out result))
                        {
                            outValue = result;
                        }
                        else
                        {
                            outValue = value;
                        }
                    }

                    break;
                default:
                    outValue = value;
                    break;
            }

            return "";
        }


        /// <summary>
        /// Serialize và chuyển sang Base64 1 object bất kỳ (object cần có thuộc tính Serializable)
        /// </summary>
        /// <param name="obj">object cần chuyển sang Base64</param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ObjectToBase64String(object obj, out string value)
        {
            string msg = "";
            value = null;

            if (obj == null) return "Error: Object is null @ObjectToBase64String";

            try
            {
                value = ObjectToBase64String(obj);
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return msg;
        }
        public static string ObjectToBase64String(object obj)
        {
            if (obj is byte[]) return Convert.ToBase64String((byte[])obj);

            return Convert.ToBase64String(ObjectToBytes(obj));
        }
        public static byte[] ObjectToBytes(object obj)
        {
            if (obj == null) return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }



    }


}
