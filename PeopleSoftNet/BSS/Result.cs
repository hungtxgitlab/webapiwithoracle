﻿using System;
using Newtonsoft.Json.Linq;

namespace BSS
{
    [Serializable]
    public class Result
    {
        public const int Ok = 0;

        public const int Error = 1;

        public const int Unauthorized = 401;

        public int Status { get; set; }

        public int StatusCode { get; set; }

        public dynamic Object { get; set; }

        public static Result ResultOk => GetResultOk();

        public bool isOk => Status == 0;

        public bool isError => Status != 0;

        public Result()
        {
        }

        public Result(int Status, dynamic obj, bool ConvertToBase64 = false, bool PascalCase = false)
        {
            this.Status = Status;
            switch (this.Status)
            {
                case 0:
                    StatusCode = 200;
                    break;
                case 1:
                    StatusCode = 400;
                    break;
                case 401:
                    StatusCode = 401;
                    break;
                default:
                    StatusCode = 202;
                    break;
            }

            if (ConvertToBase64)
            {
                string @object = null;
                Convertor.ObjectToBase64String(obj, out @object);
                Object = @object;
            }
            else if (PascalCase)
            {
                JObject original = JObject.FromObject(obj);
                //Object = original.ToCamelCase();
            }
            else
            {
                Object = (object)obj;
            }
        }

        public static Result GetResultOk(dynamic obj = null, bool ConvertToBase64 = false)
        {
            return new Result(0, obj, ConvertToBase64);
        }

        public static Result GetResultError(dynamic obj)
        {
            return new Result(1, obj);
        }

        public static Result GetResult(int Status, dynamic obj, bool ConvertToBase64 = false)
        {
            return new Result(Status, obj, ConvertToBase64);
        }
    }

    
}
