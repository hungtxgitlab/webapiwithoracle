﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSS
{
    public static class ResultStatic
    {
        public static Result ToResultOk(this int obj, bool ConvertToBase64 = false)
        {
            return new Result(0, obj, ConvertToBase64);
        }

        public static Result ToResultOk(this float obj, bool ConvertToBase64 = false)
        {
            return new Result(0, obj, ConvertToBase64);
        }

        public static Result ToResultOk(this double obj, bool ConvertToBase64 = false)
        {
            return new Result(0, obj, ConvertToBase64);
        }

        public static Result ToResultOk<T>(this T obj, bool ConvertToBase64 = false) where T : class
        {
            return new Result(0, obj, ConvertToBase64);
        }

        public static Result ToResultError<T>(this T obj, string msgError) where T : class
        {
            return new Result(1, msgError);
        }

        public static Result ToResultError(this string msgError)
        {
            return new Result(1, msgError);
        }

        //public static Result ToResultError_User(this string msgError)
        //{
        //    return new Result(1, msgError.ToMessageForUser());
        //}

        //public static Result ToResultError_System(this string msgError)
        //{
        //    return new Result(1, msgError.ToGeneralSystemErrorMessage());
        //}

        public static Result ToResult<T>(this T obj, int Status, bool ConvertToBase64 = false) where T : class
        {
            return new Result(Status, obj, ConvertToBase64);
        }
    }
}
