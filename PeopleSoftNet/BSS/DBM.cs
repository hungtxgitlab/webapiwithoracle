﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BSS
{
    public class DBM : IDisposable
    {
        public DbConnection? Connection { get; set; }
        public DbTransaction? Transaction { get; set; }
        public static string ConnectionString { get; set; }
        static DBM()
        {
            ConnectionString = Common.GetAppSettings("DB_ORACLE");
        }
        public DBM()
        {
            Connection = GetDbConnection(ConnectionString);
        }

        public static string GetOne<T>(string storeName, out T? oneItem) where T : class, new()
        {
            return GetOne<T, T>(storeName, null, out oneItem);
        }

        public static string GetOne<T, TL>(string storeName, T parameters, out TL? oneItem) where T : class where TL : class, new()
        {
            oneItem = null;
            string errorMessage = ExecuteStoredProcedure(storeName, parameters, out DataTable dataTable);

            if (errorMessage.Length > 0)
            {
                return errorMessage;
            }

            return Convertor.DataTableToObject(dataTable, out oneItem);
        }

        public static string GetList<T>(string storeName, out List<T>? outListItem) where T : class, new()
        {
            return GetList<T, T>(storeName, null, out outListItem);
        }

        public static string GetList<T, TL>(string storeName, T parameters, out List<TL>? listItem) where T : class where TL : class, new()
        {
            listItem = null;
            string errorMessage = ExecuteStoredProcedure(storeName, parameters, out DataTable dataTable);

            if (errorMessage.Length > 0) return errorMessage;

            return Convertor.DataTableToList(dataTable, out listItem);
        }

        public static string GetList<T, TL>(string storeName, T parameters, out List<TL>? listItem, out int count) where T : class where TL : class, new()
        {
            listItem = null;
            string errorMessage = ExecuteStoredProcedure(storeName, parameters, out DataTable dataTable, out count);
            if (errorMessage.Length > 0) return errorMessage;

            return Convertor.DataTableToList(dataTable, out listItem);
        }

        public void BeginTransac()
        {
            if (Connection?.State != ConnectionState.Open)
            {
                Connection?.Open();
            }

            System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.Unspecified;
            Transaction = Connection?.BeginTransaction(isolationLevel);
        }

        public void CommitTransac()
        {
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction = null;
            }

            if (Connection?.State != 0)
            {
                Connection?.Close();
            }
        }
        public void RollBackTransac()
        {
            if (Transaction != null)
            {
                Transaction.Rollback();
                Transaction = null;
            }

            if (Connection?.State != 0)
            {
                Connection?.Close();
            }
        }



        public static string Insert<T>(T data, string DestinationTableName, DBM dbm = null)
        {
            try
            {
                dbm ??= new DBM();

                // Mở kết nối nếu chưa mở
                if (dbm.Connection?.State != ConnectionState.Open)
                {
                    dbm.Connection?.Open();
                }

                // Tạo câu lệnh SQL INSERT và thêm các tham số
                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.Append($"INSERT INTO {DestinationTableName} (");
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($"{prop.Name},");
                }
                sqlCommand.Length--; // Loại bỏ dấu ',' cuối cùng
                sqlCommand.Append(") VALUES (");
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($":{prop.Name},");
                }
                sqlCommand.Length--; // Loại bỏ dấu ',' cuối cùng
                sqlCommand.Append(")");

                // Tạo command và thêm tham số
                using (OracleCommand cmd = new OracleCommand(sqlCommand.ToString(), (OracleConnection)dbm.Connection))
                {
                    // Bắt đầu transaction nếu có
                    if (dbm.Transaction != null)
                    {
                        cmd.Transaction = (OracleTransaction)dbm.Transaction;
                    }

                    // Đặt giá trị cho các tham số từ đối tượng dữ liệu được chuyển vào
                    foreach (PropertyInfo prop in properties)
                    {
                        cmd.Parameters.Add($":{prop.Name}", prop.GetValue(data));
                    }

                    // Thực thi command
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                // Trả về thông báo lỗi
                return "Lỗi khi thực hiện insert dữ liệu: " + ex.Message;
            }

            return "";
        }

        public static string Update<T>(T data, string DestinationTableName, DBM dbm = null, string condition = null)
        {
            try
            {
                dbm ??= new DBM();

                // Mở kết nối nếu chưa mở
                if (dbm.Connection?.State != ConnectionState.Open)
                {
                    dbm.Connection?.Open();
                }

                // Tạo câu lệnh SQL UPDATE và thêm các tham số
                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.Append($"UPDATE {DestinationTableName} SET ");
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($"{prop.Name} = :{prop.Name}, ");
                }
                sqlCommand.Length -= 2; // Loại bỏ dấu ',' và khoảng trắng cuối cùng

                // Thêm điều kiện WHERE
                if (!string.IsNullOrEmpty(condition))
                {
                    sqlCommand.Append($" WHERE {condition}");
                }
                else
                {
                    // Sử dụng cột primary key làm điều kiện WHERE
                    var primaryKey = GetPrimaryKey<T>(DestinationTableName, dbm);
                    if (primaryKey != null)
                    {
                        sqlCommand.Append($" WHERE {primaryKey} = :{primaryKey}");
                    }
                    else
                    {
                        throw new Exception("Không thể xác định cột primary key.");
                    }
                }

                // Tạo command và thêm tham số
                using (OracleCommand cmd = new OracleCommand(sqlCommand.ToString(), (OracleConnection)dbm.Connection))
                {
                    // Bắt đầu transaction nếu có
                    if (dbm.Transaction != null)
                    {
                        cmd.Transaction = (OracleTransaction)dbm.Transaction;
                    }

                    // Đặt giá trị cho các tham số từ đối tượng dữ liệu được chuyển vào
                    foreach (PropertyInfo prop in properties)
                    {
                        cmd.Parameters.Add($":{prop.Name}", prop.GetValue(data));
                    }

                    // Thực thi command
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                // Trả về thông báo lỗi
                return "Lỗi khi thực hiện cập nhật dữ liệu: " + ex.Message;
            }

            return "";
        }

        private static string GetPrimaryKey<T>(string DestinationTableName, DBM dbm)
        {
            string primaryKey = null;
            try
            {
                string tableName = DestinationTableName.ToUpper();

                string query = @"
            SELECT cu.COLUMN_NAME
            FROM user_cons_columns cu
            JOIN user_constraints au ON au.CONSTRAINT_NAME = cu.CONSTRAINT_NAME
            WHERE au.TABLE_NAME = :tableName AND au.CONSTRAINT_TYPE = 'P'
        ";

                using (OracleCommand cmd = new OracleCommand(query, (OracleConnection)dbm.Connection))
                {
                    cmd.Parameters.Add(":tableName", tableName);
                    primaryKey = (string)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                // Xử lý nếu có lỗi xảy ra khi truy vấn primary key
                Console.WriteLine("Lỗi khi lấy tên cột primary key: " + ex.Message);
            }

            return primaryKey;
        }



        public static string Delete<T>(T data, string DestinationTableName, DBM dbm = null)
        {
            try
            {
                dbm ??= new DBM();

                // Mở kết nối nếu chưa mở
                if (dbm.Connection?.State != ConnectionState.Open)
                {
                    dbm.Connection?.Open();
                }

                // Tạo câu lệnh SQL DELETE và thêm các tham số
                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.Append($"DELETE FROM {DestinationTableName} WHERE ");
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($"{prop.Name} = :{prop.Name} AND ");
                }
                sqlCommand.Length -= 5; // Loại bỏ ' AND ' cuối cùng

                // Tạo command và thêm tham số
                using (OracleCommand cmd = new OracleCommand(sqlCommand.ToString(), (OracleConnection)dbm.Connection))
                {
                    // Bắt đầu transaction nếu có
                    if (dbm.Transaction != null)
                    {
                        cmd.Transaction = (OracleTransaction)dbm.Transaction;
                    }

                    // Đặt giá trị cho các tham số từ đối tượng dữ liệu được chuyển vào
                    foreach (PropertyInfo prop in properties)
                    {
                        cmd.Parameters.Add($":{prop.Name}", prop.GetValue(data));
                    }

                    // Thực thi command
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                // Trả về thông báo lỗi
                return "Lỗi khi thực hiện xóa dữ liệu: " + ex.Message;
            }

            return "";
        }


        public static string BulkInsert<T>(List<T> listData, string DestinationTableName, DBM dbm = null)
        {
            try
            {
                // Khởi tạo một đối tượng DBM nếu không được truyền vào từ bên ngoài
                dbm ??= new DBM();

                // Mở kết nối nếu chưa mở
                if (dbm.Connection?.State != ConnectionState.Open)
                {
                    dbm.Connection?.Open();
                }

                // Tạo câu lệnh SQL INSERT và thêm các tham số
                StringBuilder sqlCommand = new StringBuilder();
                sqlCommand.Append($"INSERT INTO {DestinationTableName} (");
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($"{prop.Name},");
                }
                sqlCommand.Length--; // Loại bỏ dấu ',' cuối cùng
                sqlCommand.Append(") VALUES (");
                foreach (PropertyInfo prop in properties)
                {
                    sqlCommand.Append($":{prop.Name},");
                }
                sqlCommand.Length--; // Loại bỏ dấu ',' cuối cùng
                sqlCommand.Append(")");

                // Tạo command và thêm tham số
                using (OracleCommand cmd = new OracleCommand(sqlCommand.ToString(), (OracleConnection)dbm.Connection))
                {
                    // Bắt đầu transaction nếu có
                    if (dbm.Transaction != null)
                    {
                        cmd.Transaction = (OracleTransaction)dbm.Transaction;
                    }

                    // Chuyển đổi danh sách dữ liệu thành DataTable
                    string msg = Convertor.ListToDataTable(listData, out DataTable dt);
                    if (msg.Length > 0) return msg;

                    // Thêm các tham số cho mỗi hàng dữ liệu
                    foreach (DataRow row in dt.Rows)
                    {
                        // Đặt giá trị cho các tham số từ dữ liệu trong hàng
                        foreach (PropertyInfo prop in properties)
                        {
                            cmd.Parameters.Add($":{prop.Name}", row[prop.Name]);
                        }

                        // Thực thi command để chèn dữ liệu
                        cmd.ExecuteNonQuery();

                        // Xóa các tham số sau mỗi lần chèn để chuẩn bị cho hàng tiếp theo
                        cmd.Parameters.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                // Trả về thông báo lỗi
                return "Lỗi khi thực hiện chèn dữ liệu hàng loạt: " + ex.Message;
            }

            return "";
        }

        public static string BulkUpdate<T>(List<T> listData, string DestinationTableName, DBM dbm = null)
        {
            try
            {
                dbm ??= new DBM();

                if (dbm.Connection?.State != ConnectionState.Open)
                {
                    dbm.Connection?.Open();
                }

                PropertyInfo[] properties = typeof(T).GetProperties();

                StringBuilder updateCommand = new StringBuilder();
                updateCommand.Append($"UPDATE {DestinationTableName} SET ");

                // Tạo phần SET trong câu lệnh UPDATE
                foreach (PropertyInfo prop in properties)
                {
                    updateCommand.Append($"{prop.Name} = :{prop.Name},");
                }
                updateCommand.Length--; // Loại bỏ dấu ',' cuối cùng

                // Tạo điều kiện từ danh sách dữ liệu
                StringBuilder condition = new StringBuilder();
                condition.Append(" WHERE ");

                foreach (T data in listData)
                {
                    foreach (PropertyInfo prop in properties)
                    {
                        condition.Append($"{prop.Name} = :{prop.Name} OR ");
                    }
                }
                condition.Length -= 4; // Loại bỏ " OR " cuối cùng

                // Kết hợp câu lệnh UPDATE và điều kiện
                updateCommand.Append(condition);

                using (OracleCommand updateCmd = new OracleCommand(updateCommand.ToString(), (OracleConnection)dbm.Connection))
                {
                    if (dbm.Transaction != null)
                    {
                        updateCmd.Transaction = (OracleTransaction)dbm.Transaction;
                    }

                    // Thực hiện cập nhật cho mỗi đối tượng trong danh sách
                    foreach (T data in listData)
                    {
                        foreach (PropertyInfo prop in properties)
                        {
                            updateCmd.Parameters.Add($":{prop.Name}", prop.GetValue(data));
                        }
                    }

                    updateCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                return "Lỗi khi thực hiện cập nhật hàng loạt: " + ex.Message;
            }

            return "";
        }


        private static string ExecuteStoredProcedure<T>(string storeName, T parameters, out DataTable dataTable, out int total) where T : class
        {
            dataTable = new DataTable();
            total = 0;

            using (OracleConnection conn = (OracleConnection)GetDbConnection(ConnectionString))
            {
                try
                {
                    conn.Open();

                    OracleCommand cmd = conn.CreateCommand();
                    cmd.CommandText = storeName;
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Thêm tham số đầu vào cho stored procedure (nếu có)
                    if (parameters != null)
                    {
                        foreach (var prop in typeof(T).GetProperties())
                        {
                            OracleParameter param = new OracleParameter();
                            param.ParameterName = prop.Name;
                            param.Value = prop.GetValue(parameters);
                            cmd.Parameters.Add(param);
                        }
                    }

                    // Thêm tham số đầu ra cho stored procedure
                    OracleParameter outCursorParam = new OracleParameter();
                    outCursorParam.ParameterName = "o_cursor";
                    outCursorParam.OracleDbType = OracleDbType.RefCursor;
                    outCursorParam.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outCursorParam);

                    // Thêm tham số đầu ra cho tổng số bản ghi
                    OracleParameter totalParam = new OracleParameter();
                    totalParam.ParameterName = "pTotal";
                    totalParam.OracleDbType = OracleDbType.Decimal;
                    totalParam.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(totalParam);

                    // Thực thi stored procedure
                    cmd.ExecuteNonQuery();

                    // Lấy dữ liệu từ cursor và chuyển đổi thành đối tượng DataTable
                    using (OracleDataReader reader = ((OracleRefCursor)outCursorParam.Value).GetDataReader())
                    {
                        dataTable.Load(reader);
                    }

                    // Lấy giá trị tổng số bản ghi từ tham số đầu ra
                    total = ((OracleDecimal)totalParam.Value).ToInt32();

                    return "";
                }
                catch (Exception ex)
                {
                    return "Lỗi khi thực thi stored procedure: " + ex.Message;
                }
            }
        }



        private static string ExecuteStoredProcedure<T>(string storeName, T parameters, out DataTable dataTable) where T : class
        {
            dataTable = new DataTable();

            using OracleConnection conn = (OracleConnection)GetDbConnection(ConnectionString);
            try
            {
                conn.Open();

                OracleCommand cmd = conn.CreateCommand();
                cmd.CommandText = storeName;
                cmd.CommandType = CommandType.StoredProcedure;

                // Thêm tham số đầu vào cho stored procedure (nếu có)
                if (parameters != null)
                {
                    foreach (var prop in typeof(T).GetProperties())
                    {
                        OracleParameter param = new OracleParameter();
                        param.ParameterName = prop.Name;
                        param.Value = prop.GetValue(parameters);
                        cmd.Parameters.Add(param);
                    }
                }

                // Thêm tham số đầu ra cho stored procedure
                OracleParameter outCursorParam = new OracleParameter();
                outCursorParam.ParameterName = "o_cursor";
                outCursorParam.OracleDbType = OracleDbType.RefCursor;
                outCursorParam.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outCursorParam);

                // Thực thi stored procedure
                cmd.ExecuteNonQuery();

                // Lấy dữ liệu từ cursor và chuyển đổi thành đối tượng generic TL
                using (OracleDataReader reader = ((OracleRefCursor)outCursorParam.Value).GetDataReader())
                {
                    dataTable.Load(reader);
                }

                return "";
            }
            catch (Exception ex)
            {
                return "Lỗi khi thực thi stored procedure: " + ex.Message;
            }
        }


        private static DbConnection GetDbConnection(string ConnStr)
        {
            return new OracleConnection(ConnStr);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            CloseConnection();
            Connection?.Dispose();
        }

        private void CloseConnection()
        {
            if (Transaction == null)
            {
                if (Connection != null && Connection.State != ConnectionState.Closed) Connection.Close();
            }
        }
    }
}
