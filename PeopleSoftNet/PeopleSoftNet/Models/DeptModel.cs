﻿namespace PeopleSoftNet.Models
{
    public class DeptModel
    {
        public string BRANCHID { get; set; }
        public string DEPTID { get; set; }
        public string DEPTNAME { get; set; }
        public string SETID { get; set; }
        public string HOD { get; set; }
        public string BRANCHNAME { get; set; }
        public string STATUS { get; set; }
    }
}
