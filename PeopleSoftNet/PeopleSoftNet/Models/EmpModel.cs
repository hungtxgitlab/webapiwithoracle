﻿namespace PeopleSoftNet.Models
{
    public class EmpModel
    {
        public string EMPLID { get; set; }
        public string NAME { get; set; }
        public string BRANCHID { get; set; }
        public string DEPTID { get; set; }
        public string POST { get; set; }
        public string IBSY_POST_DESCR { get; set; }
        public string SEXUAL { get; set; }
        public DateTime BIRTHDATE { get; set; }
        public DateTime QUIT_DATE { get; set; }
        public string QUIT_REASON { get; set; }
        public string USER_ID { get; set; }
        public string EMAIL_ADDR { get; set; }
        public string JOBCODE { get; set; }
        public string REPORTS_TO { get; set; }
        public string HOD { get; set; }
        public string HOB { get; set; }
        public string SETID_DEPT { get; set; }
        public string JOBCODE_DESCR { get; set; }
        public string PS_JOBCODE { get; set; }
        public string PS_SETID { get; set; }
        public string IBSY_IS_HEAD_CHARG { get; set; }
        public string IBSY_JOBCODE { get; set; }
        public string IBSY_IS_HEAD_RIGHT { get; set; }
    }
}
