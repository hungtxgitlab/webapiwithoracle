﻿namespace PeopleSoftNet.ViewModels.Response
{
    public class DepartmentGetAllResponse
    {
        public string DEPTID { get; set; }
        public string DEPTNAME { get; set; }
    }
}
