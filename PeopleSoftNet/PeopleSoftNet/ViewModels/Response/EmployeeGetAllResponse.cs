﻿namespace PeopleSoftNet.ViewModels.Response
{
    public class EmployeeGetAllResponse
    {
        public int Total { get; set; }
        public List<EmployeeGetAll> ListEmployee { get; set; }
    }

    public class EmployeeGetAll
    {
        public string EMPLID { get; set; }
        public string DEPTID { get; set; }
        public string NAME { get; set; }
        public string IBSY_POST_DESCR { get; set; } //Vị tri công việc
        public string SEXUAL { get; set; }
        public DateTime BIRTHDATE { get; set; }
        public string USER_ID { get; set; }
        public string EMAIL_ADDR { get; set; }
        public string JOBCODE_DESCR { get; set; } //Mô tả công việc
        public string DEPTNAME { get; set; }
    }
}
