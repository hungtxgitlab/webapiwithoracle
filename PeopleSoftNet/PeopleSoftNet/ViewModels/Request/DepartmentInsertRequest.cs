﻿
using System.Text.Json.Serialization;

namespace PeopleSoftNet.ViewModels.Request
{
    public class DepartmentInsertRequest
    {
        [JsonIgnore]
        public string DEPTID { get; set; } = new Random().Next(100000000, 999999999).ToString();
        public string DEPTNAME { get; set; }
        public string BRANCHID { get; set; } = "000000000";
    }
}
