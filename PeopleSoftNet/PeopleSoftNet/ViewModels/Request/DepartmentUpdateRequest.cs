﻿namespace PeopleSoftNet.ViewModels.Request
{
    public class DepartmentUpdateRequest
    {
        public string DEPTID { get; set; }
        public string DEPTNAME { get; set; }
    }
}
