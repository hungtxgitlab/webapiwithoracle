﻿using BSS;
using System.ComponentModel.DataAnnotations;

namespace PeopleSoftNet.ViewModels.Request
{
    public class PaginationRequestVM
    {
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public string TextSearch { get; set; }
    }
}
