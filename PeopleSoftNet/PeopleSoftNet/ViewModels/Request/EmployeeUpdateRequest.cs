﻿namespace PeopleSoftNet.ViewModels.Request
{
    public class EmployeeUpdateRequest
    {
        public string EMPLID { get; set; } 
        public string DEPTID { get; set; }
        public string NAME { get; set; }
        public string IBSY_POST_DESCR { get; set; } //Vị tri công việc
        public string SEXUAL { get; set; }
        public DateTime BIRTHDATE { get; set; }
        public string USER_ID { get; set; }
        public string EMAIL_ADDR { get; set; }
        public string JOBCODE_DESCR { get; set; } //Mô tả công việc
    }
}
