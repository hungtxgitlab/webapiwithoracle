﻿namespace PeopleSoftNet.ViewModels.Request
{
    public class GetListEmployeeRequest : PaginationRequestVM
    {
        public string DEPTID { get; set; }
    }
}
