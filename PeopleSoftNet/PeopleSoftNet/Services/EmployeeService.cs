﻿using BSS;
using PeopleSoftNet.Models;
using PeopleSoftNet.ViewModels.Request;
using PeopleSoftNet.ViewModels.Response;

namespace PeopleSoftNet.Services
{
    public interface IEmployeeService
    {
        public string GetListByDepartmentID(GetListEmployeeRequest request, out EmployeeGetAllResponse result);
        public string GetDetailsByID(string EMPLID, out EmpModel result);
        public string InsertEmployee(EmployeeInsertRequest request);
        public string UpdateEmployee(EmployeeUpdateRequest request);
        public string DeleteEmployee(string EMPLID);

    }

    public class EmployeeService : IEmployeeService
    {
        public string GetListByDepartmentID(GetListEmployeeRequest request, out EmployeeGetAllResponse result)
        {
            result = new();

            string msg = DBM.GetList("sp_emp_GetAllEmp", request, out List<EmployeeGetAll> list, out int total);
            if (msg.Length > 0) return msg;

            result.ListEmployee = list;
            result.Total = total;
                
            return "";
        }

        public string GetDetailsByID(string Emplid, out EmpModel result)
        {
            return DBM.GetOne("sp_emp_GetDetailsByID", new { Emplid }, out result);
        }

        public string InsertEmployee(EmployeeInsertRequest request)
        {
            return DBM.Insert(request, "EMP");
        }

        public string UpdateEmployee(EmployeeUpdateRequest request)
        {
            return DBM.Update(request, "EMP");
        }

        public string DeleteEmployee(string EMPLID)
        {
            return DBM.Delete(new { EMPLID }, "EMP");
        }

        //public string InsertEmployee(EmpModel request)
        //{
        //    DBM dbm = new();
        //    dbm.BeginTransac();

        //    string msg = DBM.Insert(request, "EMP", dbm);
        //    if (msg.Length > 0) { dbm.RollBackTransac(); return msg; }

        //    msg = DBM.Insert(new { DEPTID = "Hung", BRANCHID = "Hung2" }, "C_DEPT", dbm);
        //    if (msg.Length > 0) { dbm.RollBackTransac(); return msg; }

        //    dbm.CommitTransac();

        //    return "";
        //}


        //public string BulkInsertEmployee(List<EmpModel> request)
        //{
        //    DBM dbm = new();
        //    dbm.BeginTransac();

        //    string msg = DBM.BulkInsert(request, "EMP", dbm);
        //    if (msg.Length > 0) { dbm.RollBackTransac(); return msg; }

        //    msg = DBM.Insert(new { DEPTID = "Hung", BRANCHID = "Hung2" }, "C_DEPT", dbm);
        //    if (msg.Length > 0) { dbm.RollBackTransac(); return msg; }

        //    dbm.CommitTransac();

        //    return "";
        //}
    }
}
