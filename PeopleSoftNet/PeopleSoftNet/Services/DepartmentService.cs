﻿using BSS;
using PeopleSoftNet.ViewModels.Request;
using PeopleSoftNet.ViewModels.Response;

namespace PeopleSoftNet.Services
{
    public interface IDepartmentService
    {
        public string GetAllDepartment(out List<DepartmentGetAllResponse> result);
        public string InsertDepartment(DepartmentInsertRequest request);
        public string UpdateDepartment(DepartmentUpdateRequest request);
        public string DeleteDepartment(string DEPTID);

    }
    public class DepartmentService: IDepartmentService
    {
        public string DeleteDepartment(string DEPTID)
        {
            return DBM.Delete(new { DEPTID }, "C_DEPT");
        }

        public string GetAllDepartment(out List<DepartmentGetAllResponse> result)
        {
            return DBM.GetList("sp_emp_GetAllDepartment", out result);
        }

        public string InsertDepartment(DepartmentInsertRequest request)
        {
            return DBM.Insert(request, "C_DEPT");
        }

        public string UpdateDepartment(DepartmentUpdateRequest request)
        {
            return DBM.Update(request, "C_DEPT");
        }
    }
}
