﻿using BSS;
using Microsoft.AspNetCore.Mvc;
using PeopleSoftNet.Models;
using PeopleSoftNet.Services;
using PeopleSoftNet.ViewModels.Request;
using PeopleSoftNet.ViewModels.Response;

namespace PeopleSoftNet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _iDepartmentService;

        public DepartmentController(IDepartmentService iDepartmentService)
        {
            _iDepartmentService = iDepartmentService;
        }

        /// <summary>
        /// Hiển thị danh sách phòng ban
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("GetAllDepartment")]
        public Result GetAllDepartment()
        {
            string msg = _iDepartmentService.GetAllDepartment(out List<DepartmentGetAllResponse> result);
            if (msg.Length > 0) return msg.ToResultError();
            return result.ToResultOk();
        }

        [HttpPost("InsertDepartment")]
        public Result InsertDepartment([FromBody] DepartmentInsertRequest request)
        {
            string msg = _iDepartmentService.InsertDepartment(request);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }

        [HttpPost("UpdateDepartment")]
        public Result UpdateDepartment([FromBody] DepartmentUpdateRequest request)
        {
            string msg = _iDepartmentService.UpdateDepartment(request);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }


        [HttpPost("DeleteDepartment")]
        public Result DeleteDepartment(string DEPTID)
        {
            string msg = _iDepartmentService.DeleteDepartment(DEPTID);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }


    }
}
