﻿using BSS;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PeopleSoftNet.Models;
using PeopleSoftNet.Services;
using PeopleSoftNet.ViewModels.Request;
using PeopleSoftNet.ViewModels.Response;

namespace PeopleSoftNet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _iEmployeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _iEmployeeService = employeeService;
        }

     
        /// <summary>
        /// Hiển thị phân trang danh sách nhân viên theo phòng ban
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("GetListByDepartmentID")]
        public Result GetListByDepartmentID(GetListEmployeeRequest request)
        {
            string msg = _iEmployeeService.GetListByDepartmentID(request, out EmployeeGetAllResponse result);
            if (msg.Length > 0) return msg.ToResultError();
            return result.ToResultOk();
        }


        /// <summary>
        /// Hiển thị chi tiết nhân viên
        /// </summary>
        /// <param name="EMPLID"></param>
        /// <returns></returns>
        [HttpGet("GetDetailsByID")]
        public Result GetDetailsByID(string EMPLID)
        {
            string msg = _iEmployeeService.GetDetailsByID(EMPLID, out EmpModel result);
            if (msg.Length > 0) return msg.ToResultError();
            return result.ToResultOk();
        }


        [HttpPost("InsertEmployee")]
        public Result InsertEmployee(EmployeeInsertRequest request)
        {
            string msg = _iEmployeeService.InsertEmployee(request);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }

        [HttpPost("UpdateEmployee")]
        public Result UpdateEmployee(EmployeeUpdateRequest request)
        {
            string msg = _iEmployeeService.UpdateEmployee(request);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }


        [HttpPost("DeleteEmployee")]
        public Result DeleteEmployee(string Emplid)
        {
            string msg = _iEmployeeService.DeleteEmployee(Emplid);
            if (msg.Length > 0) return msg.ToResultError();
            return "".ToResultOk();
        }


        //[HttpPost("BulkInsertEmployee")]
        //public Result BulkInsertEmployee(List<EmpModel> request)
        //{
        //    string msg = _iEmployeeService.BulkInsertEmployee(request);
        //    if (msg.Length > 0) return msg.ToResultError();
        //    return "".ToResultOk();
        //}
    }



}
