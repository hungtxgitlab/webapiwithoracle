﻿using PeopleSoftNet.Services;

namespace PeopleSoftNet.ServiceRegistration
{
    public static class ServiceRegistration
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IEmployeeService, EmployeeService>();

        }
    }
}
